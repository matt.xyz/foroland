import { Injectable } from '@angular/core';
import { IPublicacion } from '../models/publicacion.interface';

@Injectable({
  providedIn: 'root'
})
export class PublicacionService {
  post:IPublicacion[]=[];
  constructor() {
    this.post=[
      {titulo:'MOODs de Minecraft',fecha:new Date(),descripcion:'Los antiguos y nuevos moods de minecraft son muy interesante y aunque hay gente en contra siempre estare a favor de los clasicos.'},
      {titulo:'Biomas',fecha:new Date(),descripcion:'¿Cual es su bioma favorito? :)'}
    ];
  }

  obtenerPost(){
    return this.post;
  }

  addPost(publicacion:IPublicacion){
    this.post.push(publicacion);
    return false;
  }
}
